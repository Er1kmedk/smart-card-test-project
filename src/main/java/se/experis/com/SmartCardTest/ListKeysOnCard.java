//package se.experis.com.SmartCardTest;
//
//import javax.smartcardio.Card;
//import javax.smartcardio.CardTerminal;
//import javax.smartcardio.TerminalFactory;
//import java.io.ByteArrayInputStream;
//import java.nio.charset.Charset;
//import java.security.KeyStore;
//import java.security.Provider;
//import java.security.Security;
//import java.util.Collections;
//import java.util.List;
//import java.util.Objects;
//import java.util.stream.Collectors;
//import java.util.stream.IntStream;
//
//public class ListKeysOnCard {
//
//    public static final String PATH_TO_NATIVE_DLL = "C:\\Program Files\\OpenSC Project\\OpenSC\\pkcs11\\opensc-pkcs11.dll";
//
//    public static void main(String[] args) {
//
//        try {
//
//            List<CardSlot> cardSlots = installProviders();
//
//            for (CardSlot card : cardSlots) {
//                // This class needs to be created in the CardSlot class (hidden code)
////                System.out.println("Keys on card: " + card.getDisplayName());
//
//                for (String alias : getCardAliases(card)) {
//                    System.out.println("-->Key: " + alias);
//                }
//            }
//
//        } catch (Exception e) {
//
//        }
//    }
//
//    public static List<CardTerminal> getCardTerminals() throws Exception {
//        TerminalFactory tFactory = TerminalFactory.getDefault();
//
//        return tFactory.terminals().list();
//    }
//
//    public static  List<CardSlot> installProviders() throws Exception {
//        List<CardTerminal> cardReaders = getCardTerminals();
//
//        // This syntax might cause problem due to us having a different file structure
//        return IntStream.range(0, cardReaders.size()).mapToObj(i -> {
//            CardSlot newSlot = null;
//
//            try {
//                if (cardReaders.get(i).isCardPresent()) {
//                    String config = String.format("name=%1$\n"
//                                    + "library=\"%2$s\"\n"
//                                    + "slot=%3$d",
//                            String.format("PKCS11-%1$d", i),
//                            PATH_TO_NATIVE_DLL.replace("\\", "\\\\"),
//                            i);
//
//                    ByteArrayInputStream is = new ByteArrayInputStream(
//                            config.getBytes(Charset.defaultCharset()));
//
//                    Provider p = new sun.security.pkcs11.SunPKCS11(is);
//
//                    Security.addProvider(p);
//
//                    newSlot = new CardSlot(p.getName(), cardReaders.get(i), i);
//                }
//            }
//            catch (Exception e) {
//                e.printStackTrace();
//            }
//            return newSlot;
//        }).filter(cs -> Objects.nonNull(cs)).collect(Collectors.toList());
//    }
//
////    public static List<String> getCardAliases(CardSlot slot) throws Exception {
////        KeyStore.Builder builder = KeyStore.Builder.newInstance(
////                "PKCS11",
////                Security.getProvider((slot.getProviderName)),
////                new KeyStore.CallbackHandlerProtection(new AuthHandler()));
////
////        KeyStore smartCard = builder.getKeyStore();
////
////        return Collections.list(smartCard.aliases());
////    }
//
//}
