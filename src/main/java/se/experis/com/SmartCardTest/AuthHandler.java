package se.experis.com.SmartCardTest;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.swing.*;
import java.io.IOException;

public class AuthHandler implements CallbackHandler {

    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        for (Callback callback: callbacks) {
            if(callback instanceof PasswordCallback) {
                PasswordCallback pcb = (PasswordCallback) callback;

                JPanel panel = new JPanel();
                JLabel label = new JLabel("Enter Pin: ");
                JPasswordField pass = new JPasswordField(10);

                panel.add(label);
                panel.add(pass);

                String[] options = new String[] {"OK", "Cancel"};

                int option = JOptionPane.showOptionDialog(
                        null,
                        panel, "CAC Pin Required",
                        JOptionPane.NO_OPTION,
                        JOptionPane.PLAIN_MESSAGE,
                        null,
                        options,
                        options[1]);

                if (option == 0) {
                    pcb.setPassword(pass.getPassword());
                }
            }
        }
    }
}
