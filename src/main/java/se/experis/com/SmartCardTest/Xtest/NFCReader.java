package se.experis.com.SmartCardTest.Xtest;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

public class NFCReader extends MifareUltraLightCUtil{

    public static final String PROPERTY_TYPE_TAG_IDENTITY= "identity";
    public static final String PROPERTY_TYPE_TAG_VALUE= "value";


    public Properties read() throws NFCException, CardException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        CardChannel channel = findCardChannel();
        String identity = readCardIdentity(channel);
        System.out.println(channel);
        String value = read(channel);
        channel.getCard().disconnect(false);
        Properties result = new Properties();
        result.put(PROPERTY_TYPE_TAG_IDENTITY, identity);
        result.put(PROPERTY_TYPE_TAG_VALUE, value);
        return result;
    }





}