package se.experis.com.SmartCardTest.Xtest;

import java.applet.Applet;
import java.util.Date;
import java.util.Properties;

public class NFCReaderApplet extends Applet {

    private Properties tagContent=new Properties();
//    private JSObject jso=null;

    public static void main(String[] args) {

        NFCReaderApplet test = new NFCReaderApplet();
        NFCReader reader = new NFCReader();
        test.readCard(reader);
    }

    private void readCard(NFCReader reader){
        try{
            System.out.println(reader.read() + "hej");
            Properties card = reader.read();
            String tagID = (String) card.get(NFCReader.PROPERTY_TYPE_TAG_IDENTITY);
            String tagValue = (String) card.get(NFCReader.PROPERTY_TYPE_TAG_VALUE);
            if (! card.equals(tagContent)) {
                tagContent=card;
                System.out.println(new Date().toString() + " .... tag wurde gelesen:");

                String result="{ id: '" + tagID + "',content: '" + tagValue + "' }";
//                jso.call(callback,new String[]{result});
                System.out.println(result);
            }
        }catch (Exception e) {
            tagContent=new Properties();
        }
    }
}
