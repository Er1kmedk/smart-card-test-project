package se.experis.com.SmartCardTest.Xtest;

import java.io.Serializable;

public class NFCException extends Exception implements Serializable {
    private String localizedMessage="";
    /**
     * constructor<
     */
    public NFCException() {
        super();
    }

    /**
     * constructor
     * @param s
     */
    public NFCException(String s) {
        super(s);
    }
    /**
     * constructor
     * @param s
     */
    public NFCException(String s,String translatedMessage) {
        super(s);
        this.localizedMessage=translatedMessage;
    }

    @Override
    public String getLocalizedMessage() {
        return localizedMessage;
    }

}
