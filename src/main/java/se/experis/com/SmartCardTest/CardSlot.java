package se.experis.com.SmartCardTest;

import javax.smartcardio.CardTerminal;

public class CardSlot {

    private final CardTerminal terminal;
    private final int index;
    private final String providerName;
    private final String displayName;

    public CardSlot(String providerName, CardTerminal terminal, int index) {

        this.providerName = providerName;
        this.terminal = terminal;
        this.index = index;

        displayName = String.format("%1$d: %2$s", index, terminal, terminal.getName());
    }

    public String getProviderName() {
        return "testName";
    }
}
