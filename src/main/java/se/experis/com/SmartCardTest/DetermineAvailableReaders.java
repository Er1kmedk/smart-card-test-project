package se.experis.com.SmartCardTest;

import javax.smartcardio.*;
import java.util.List;

public class DetermineAvailableReaders {
    public static void main(String[] args) {
        System.out.println("Active card readers: ");

        try {
            DetermineAvailableReaders dar = new DetermineAvailableReaders();
            byte[] readIdentityAPDU = {(byte) 0xFF, (byte) 0xCA, (byte) 0x00, (byte) 0x00, (byte) 0x00};



//            System.out.println("Card reader: " + ourCardReader);
//            System.out.println("Card inserted: " + ourCardReader.isCardPresent() + "\n");


                CardTerminal cardReader = null;
            for(CardTerminal cardReadercheck: getCardTerminals()) {
                if (cardReadercheck.getName().equals("ACS ACR1252 1S CL Reader PICC 0")) {
                    cardReader = cardReadercheck;
                    break;
                }
            }
            if (cardReader == null) {


            }
            else {
                System.out.println("Reader name: " + cardReader.getName());
                System.out.println("Card inserted: " + cardReader.isCardPresent());


                System.out.println(getCardTerminals());
                if (cardReader.isCardPresent()) {
                    Card card = cardReader.connect("*");
                    System.out.println("Card: " + card);

                    CardChannel channel = card.getBasicChannel();
                    System.out.println("Channel: " + channel);
                    ResponseAPDU r = channel.transmit(new CommandAPDU(readIdentityAPDU));

                    System.out.println("Response: " + r.getBytes().length);
                    System.out.println(r.getBytes());
//                    byte[] bytes = r.getBytes();0
//                    String test = Base64.getEncoder().encodeToString(bytes);
//                    System.out.println(test);
//                    String idString = dar.byteArrayToHexString(r.getData());
//                    String idStart = idString.substring(0,6);
//                    String idStop = idString.substring(8);
//                    System.out.println(idStart + idStop);
//                    System.out.println(idStop);
                }

                System.out.println("\n");


            }

        } catch (Exception e ) {
            e.printStackTrace();
        }

    }



    public static List<CardTerminal> getCardTerminals() throws Exception {
        TerminalFactory tFactory = TerminalFactory.getDefault();

        return tFactory.terminals().list();
    }

//    public String readCardIdentity(CardChannel channel) throws CardException{
//        byte[] readIdentityAPDU = {(byte) 0xFF, (byte) 0xCA, (byte) 0x00, (byte) 0x00, (byte) 0x00};
//        ResponseAPDU response = channel.transmit(new CommandAPDU(readIdentityAPDU));
//        String idString = byteArrayToHexString(response.getData());
//        String idStart = idString.substring(0,6);
//        String idStop = idString.substring(8);
//        return idStart+idStop;
//    }

//    public String byteArrayToHexString(byte[] b) {
//        StringBuffer sb = new StringBuffer(b.length * 2);
//        for (int i = 0; i < b.length; i++) {
//            int v = b[i] & 0xff;
//            if (v < 16) {
//                sb.append('0');
//            }
//            sb.append(Integer.toHexString(v));
//        }
//        return sb.toString().toUpperCase();
//    }
}
