package se.experis.com.SmartCardTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartCardTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartCardTestApplication.class, args);
	}

}
