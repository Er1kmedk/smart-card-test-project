package se.experis.com.SmartCardTest.theFinalSolution;

import javax.smartcardio.ATR;
import javax.smartcardio.Card;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import java.util.List;

public class ThreadSeparatedCardScanner implements Runnable {

    // TODO If card reader is disconnected while the program is running, a CardException is thrown
    // from the waitForCard() and the waitForCardAbsent function. Catch this and ensure the program dont crash.

    // Create socket

    CardTerminal cardReader;
    private boolean isInatiated = false;


    public ThreadSeparatedCardScanner() {
        try {
            for (CardTerminal cardReader: getCardTerminals()) {
                if (cardReader.getName().equals("ACS ACR1252 1S CL Reader PICC 0")) {
                    this.cardReader = cardReader;
                    System.out.println("Now using card reader: " + cardReader);
                    isInatiated = true;
                    break;
                } else {
                    System.err.println("Could not find the correct card reader - Make sure it is properly connected!");
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isInatiated() {
        return isInatiated;
    }

    @Override
    public void run() {
        try {
            while (true) {
                cardReader.waitForCardPresent(0);
                try {
                    Card card = cardReader.connect("*");
                    System.out.println("Card connected: " + card);
                } catch (Exception e) {
                    // send error message to frontend
                    System.err.println("Error when reading card - Please remove the card and try again!");
                }
                // socket.emit
                cardReader.waitForCardAbsent(0);
                System.out.println("Card disconnected! \n");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<CardTerminal> getCardTerminals() throws Exception {
        TerminalFactory tFactory = TerminalFactory.getDefault();

        return tFactory.terminals().list();
    }
}
